# Producer Consumer

**Producer Consumer** is a is a project grouping different use cases of EIP using Apache Camel.

#### ProducerConsumer1
Send messages to the consumer from the standard input.

#### ProducerConsumer2
Send messages to different consumers from the standard input, the consumer is chosen according to the content of the message (using header).

#### ProducerConsumer3
Integrate a web service (**ZooManager**).

#### ProducerConsumer4
Integrate a web service (**GeoNames**).

# ZOO MANAGER

**Zoo Manager** is an Restful API used to manage a zoo, operating with EIP.

## Features

### Expected Features
|**METHOD**|**URL**|**BODY**|**DESCRIPTION**|
| ------------ | ------------ | ------------ | ------------ |
|**GET**|/animals| |Returns all of the animals of the center|
|**POST**|/animals|**Animal**|Adds an animal to your center|
|**GET**|/animals/{animal_id}|   |Returns the animal identified by {animal_id}|
|**PUT**|/animals|**Animal**|Edits all animals|
|**DELETE**|/animals|   |Deletes all animals|
|**POST**|/animals/{animal_id}|**Animal**|Creates the animal identified by {animal_id}|
|**PUT**|/animals/{animal_id}|**Animal**|Edits the animal identified by {animal_id}|
|**DELETE**|/animals/{animal_id}|   |Deletes the animal identified by {animal_id}|
|**GET**|/find/byName/{animal_name}|   |Searches for an animal by name|
|**GET**|/find/at/{position}|   |Searches for an animal by position|
|**GET**|/find/near/{position}|   |Searches animals near a position (within a radius of 100 km)|
|**GET**|/animals/{animal_id}/wolf|   |Retrieves informations on the animal identified by {animal_id} using the [WolframAlpha][1] service|
|**GET**|/center/journey/from/{route}|   |Retrieves route information from a GPS location to your center using the [GraphHopper][2] service|

### Additional Features

|**METHOD**|**URL**|**BODY**|**DESCRIPTION**|
| ------------ | ------------ | ------------ | ------------ |
|**POST**|/cages|**Cage**|Adds a cage to your center|
|**GET**|/cages/{cage_name}|   |Returns the cage named {cage_name}|
|**PUT**|/cages|**Cage**|Edits all cages|
|**DELETE**|/cages|   |Deletes all cages|
|**PUT**|/cages/{cage_name}|**Cage**|Edits the cage named {cage_name}|
|**DELETE**|/cages/{cage_name}|   |Deletes the cage named {cage_name}|

### Attributes Format

- **animal_id** : `^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$`
- **position** : `^lat=\d*.\d*&lng=\d*.\d*$`
- **route** : `^lat=\d*.\d*&lng=\d*.\d*(&vehicle=(car|small_truck|truck|scooter|foot|hike|bike|mtb|racingbike))?$`
- **animal_name** : `^.*$`
- **cage_name** : `^.*$`

### Examples

- **Get all of the animals of the center :**
```
localhost:8080/animals
```

- **Get the animal identified by a specific UUID :**
```
localhost:8080/animals/41bcfc5c-49ea-453b-a8c9-e104a0343e11
```

- **Search for an animal named Tic :**
```
localhost:8080/find/byName/Tic
```

- **Search for an animal at Rouen :**
```
localhost:8080/find/at/lat=49.443889&lng=1.103333
```

- **Search animals near Rouen (within a radius of 100 km) :**
```
localhost:8080/find/near/lat=49.443889&lng=1.103333
```

- **Get informations about an animal identified by a specific UUID :**
```
localhost:8080/animals/a0264613-bfe9-46ca-bb33-70f4fd3d637d/wolf
```

- **Get route information from Rouen to the center (car by default) :**
```
localhost:8080/center/journey/from/lat=49.443889&lng=1.103333
```

- **Get route information from Rouen to the center using a bike :**
```
localhost:8080/center/journey/from/lat=49.443889&lng=1.103333&vehicle=bike
```

- **Get the cage named Rouen :**
```
localhost:8080/cages/Rouen
```

## Setting up

1. Put the path of your different [**ZooManager**](https://gitlab.com/Romualdinho/TP-CLOUD) web services in [**CamelRoutes.java**](/ZOO_MANAGER/src/main/java/tp/camel/CamelRoutes.java)
1. Run [**SpringBootCamelIntegrationApplication**](/ZOO_MANAGER/src/main/java/tp/SpringBootCamelIntegrationApplication.java) to run the server
1. Run [**MyClient**](https://gitlab.com/ad2018/TP-EIP/blob/master/ZOO_MANAGER/src/main/java/tp/rest/MyClient.java) if you want to use a client to interact with the service

**Service is running on 8080 port by default.**.

## Built With

- [Java 8](https://www.java.com) &mdash; A computer programming language
- [SpringBoot](https://spring.io/) &mdash; An application framework and inversion of control container for the Java platform
- [Apache Camel](http://camel.apache.org/) &mdash; An open source framework for message-oriented middleware with a rule-based routing and mediation engine that provides a Java object-based implementation of the EIP
- [ZooManager](https://zoo-manager-rest.eu-gb.mybluemix.net/zoo-manager/animals) &mdash; An Restful API used to manage a zoo
- [JAX-RS](https://en.wikipedia.org/wiki/Java_API_for_RESTful_Web_Services) &mdash; A Java programming language API for creating REST web services


## Contributors

The original contributors can be found in [**AUTHORS.MD**](/AUTHORS.md).
