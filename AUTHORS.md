# Developers

* [Romuald Leroux](//gitlab.com/Romualdinho) - Developer
* [Yohann Lebidois](//gitlab.com/y.lebidois) - Developer

### Special Thanks To

* [Arnaud Saval](//gitlab.com/asaval) - Professor at the University of Rouen, original developer of this service
* [Clément Caron](//gitlab.com/ccaron) - Professor at the University of Rouen, original developer of this service