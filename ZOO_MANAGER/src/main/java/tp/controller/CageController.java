package tp.controller;

import org.apache.camel.FluentProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tp.model.Cage;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
public class CageController {

    @Autowired
    FluentProducerTemplate template;

    /**
     * GET method bound to calls on /cages/{something}
     */
    @RequestMapping(path = "/cages/{cage_name}", method = GET, produces = APPLICATION_JSON_VALUE)
    public Cage findByName(@PathVariable String cage_name) {
        return template.withProcessor(exchange -> exchange.setProperty("name", cage_name))
                .to("direct:FindCageByName").request(Cage.class);
    }

    /**
     * POST method bound to calls on /cages
     */
    @RequestMapping(path = "/cages", method = POST, consumes = APPLICATION_XML_VALUE)
    public void createCage(@RequestBody Cage cage) {
        template.withBody(cage).to("direct:CreateCage").request();
    }

    /**
     * PUT method bound to calls on /cages/{something}
     */
    @RequestMapping(path = "/cages/{cage_name}", method = PUT, consumes = APPLICATION_JSON_VALUE)
    public void updateByName(@PathVariable String cage_name, @RequestBody Cage cage) {
        template.withBody(cage).withProcessor(exchange -> exchange.setProperty("name", cage_name))
                .to("direct:UpdateCageByName").request();
    }

    /**
     * PUT method bound to calls on /cages
     */
    @RequestMapping(path = "/cages", method = PUT, consumes = APPLICATION_JSON_VALUE)
    public void updateAll(@RequestBody Cage cage) {
        template.withBody(cage)
                .to("direct:UpdateAllCages").request();
    }

    /**
     * DELETE method bound to calls on /cages/{something}
     */
    @RequestMapping(path = "/cages/{cage_name}", method = DELETE)
    public void deleteByName(@PathVariable String cage_name) {
        template.withProcessor(exchange -> exchange.setProperty("name", cage_name))
                .to("direct:DeleteCageByName").request();
    }

    /**
     * DELETE method bound to calls on /cages
     */
    @RequestMapping(path = "/cages", method = DELETE)
    public void deleteAll() {
        template.to("direct:UpdateAllCages").request();
    }
}
