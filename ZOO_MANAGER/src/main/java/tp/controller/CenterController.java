package tp.controller;

import org.apache.camel.FluentProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tp.model.Cage;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class CenterController {

    @Autowired
    FluentProducerTemplate template;

    /**
     * GET method bound to calls on /center/journey/from/{route}
     */
    @RequestMapping(path = "/center/journey/from/{center_route}", method = GET, produces = APPLICATION_XML_VALUE)
    public String getRouteInformation(@PathVariable String center_route) {
        return template.withProcessor(exchange -> exchange.setProperty("route", center_route))
                .to("direct:GetRouteInformation").request(String.class);
    }
}
