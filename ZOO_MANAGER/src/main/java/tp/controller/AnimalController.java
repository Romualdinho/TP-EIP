package tp.controller;

import org.apache.camel.FluentProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
public class AnimalController {

    @Autowired
    FluentProducerTemplate template;

    /**
     * GET method bound to calls on /animals
     */
    @RequestMapping(path = "/animals", method = GET, produces = APPLICATION_JSON_VALUE)
    public Center findAll() {
        return template.to("direct:FindAllAnimals").request(Center.class);
    }

    /**
     * GET method bound to calls on /animals/{something}
     */
    @RequestMapping(path = "/animals/{animal_id}", method = GET, produces = APPLICATION_JSON_VALUE)
    public Animal findById(@PathVariable UUID animal_id) {
        return template.withProcessor(exchange -> exchange.setProperty("id", animal_id))
                .to("direct:FindAnimalById").request(Animal.class);
    }

    /**
     * GET method bound to calls on /find/byName{something}
     */
    @RequestMapping(path = "/find/byName/{animal_name}", method = GET, produces = APPLICATION_JSON_VALUE)
    public Animal findByName(@PathVariable String animal_name) {
        return template.withProcessor(exchange -> exchange.setProperty("name", animal_name))
                .to("direct:FindAnimalByName").request(Animal.class);
    }

    /** ERROR
     * GET method bound to calls on /find/at/{something}
     */
    @RequestMapping(path = "/find/at/{animal_position}", method = GET, produces = APPLICATION_JSON_VALUE)
    public Animal findByPosition(@PathVariable String animal_position) {
        return template.withProcessor(exchange -> exchange.setProperty("position", animal_position))
                .to("direct:FindAnimalAtPosition").request(Animal.class);
    }

    /**
     * GET method bound to calls on /find/near/{something}
     */
    @RequestMapping(path = "/find/near/{animal_position}", method = GET, produces = APPLICATION_JSON_VALUE)
    public Cage findNearPosition(@PathVariable String animal_position) {
        return template.withProcessor(exchange -> exchange.setProperty("position", animal_position))
                .to("direct:FindAnimalNearPosition").request(Cage.class);
    }

    /**
     * GET method bound to calls on /animals/{something}/wolf
     */
    @RequestMapping(path = "/animals/{animal_id}/wolf", method = GET, produces = APPLICATION_XML_VALUE)
    public String getInformation(@PathVariable UUID animal_id) {
        return template.withProcessor(exchange -> exchange.setProperty("id", animal_id))
                .to("direct:GetInformation").request(String.class);
    }

    /**
     * POST method bound to calls on /animals
     */
    @RequestMapping(path = "/animals", method = POST, consumes = APPLICATION_XML_VALUE)
    public void createAnimal(@RequestBody Animal animal) {
        template.withBody(animal).to("direct:CreateAnimal").request();
    }

    /**
     * PUT method bound to calls on /animals/{something}
     */
    @RequestMapping(path = "/animals/{animal_id}", method = PUT, consumes = APPLICATION_XML_VALUE)
    public void updateById(@PathVariable UUID animal_id, @RequestBody Animal animal) {
        template.withBody(animal).withProcessor(exchange -> exchange.setProperty("id", animal_id))
                .to("direct:UpdateAnimalById").request();
    }

    /**
     * PUT method bound to calls on /animals
     */
    @RequestMapping(path = "/animals", method = PUT, consumes = APPLICATION_XML_VALUE)
    public void updateAll(@RequestBody Animal animal) {
        template.withBody(animal)
                .to("direct:UpdateAllAnimals").request();
    }

    /**
     * DELETE method bound to calls on /animals/{something}
     */
    @RequestMapping(path = "/animals/{animal_id}", method = DELETE)
    public void deleteById(@PathVariable UUID animal_id) {
        template.withProcessor(exchange -> exchange.setProperty("id", animal_id))
                .to("direct:DeleteAnimalById").request();
    }

    /**
     * DELETE method bound to calls on /animals
     */
    @RequestMapping(path = "/animals", method = DELETE)
    public void deleteAll() {
        template.to("direct:DeleteAllAnimals").request();
    }
}