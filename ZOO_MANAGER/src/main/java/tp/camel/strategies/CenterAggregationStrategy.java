package tp.camel.strategies;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import tp.model.Center;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;

public class CenterAggregationStrategy implements AggregationStrategy {
    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        try {
            if (oldExchange != null) {
                return oldExchange;
            }

            Center c;
            if (newExchange.getIn().getBody(Center.class) != null) {
                c = newExchange.getIn().getBody(Center.class);
            } else {
                c = unmarshalCenter(newExchange.getIn().getBody(String.class));
            }
            newExchange.getIn().setBody(c);
            return newExchange;

        } catch (JAXBException e) {
            e.printStackTrace();
            return newExchange;
        }
    }

    private Center unmarshalCenter(String xml) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(Center.class);
        return (Center) jc.createUnmarshaller().unmarshal(new StreamSource(new StringReader(xml)));
    }
}