package tp.camel.strategies;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import tp.model.Animal;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;

public class AnimalAggregationStrategy implements AggregationStrategy {
    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        try {
            if (oldExchange != null) {
                return oldExchange;
            }

            Animal a;
            if (newExchange.getIn().getBody(Animal.class) != null) {
                a = newExchange.getIn().getBody(Animal.class);
            } else {
                a = unmarshalAnimal(newExchange.getIn().getBody(String.class));
            }
            newExchange.getIn().setBody(a);
            return newExchange;

        } catch (JAXBException e) {
            e.printStackTrace();
            return newExchange;
        }
    }

    private Animal unmarshalAnimal(String xml) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(Animal.class);
        return (Animal) jc.createUnmarshaller().unmarshal(new StreamSource(new StringReader(xml)));
    }
}