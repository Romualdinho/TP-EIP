package tp.camel.strategies;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import tp.model.Cage;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;

public class CageAggregationStrategy implements AggregationStrategy {
    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        try {
            if (oldExchange != null) {
                return oldExchange;
            }

            Cage c;
            if (newExchange.getIn().getBody(Cage.class) != null) {
                c = newExchange.getIn().getBody(Cage.class);
            } else {
                c = unmarshalCage(newExchange.getIn().getBody(String.class));
            }
            newExchange.getIn().setBody(c);
            return newExchange;

        } catch (JAXBException e) {
            e.printStackTrace();
            return newExchange;
        }
    }

    private Cage unmarshalCage(String xml) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(Cage.class);
        return (Cage) jc.createUnmarshaller().unmarshal(new StreamSource(new StringReader(xml)));
    }
}