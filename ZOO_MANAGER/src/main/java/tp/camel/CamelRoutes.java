package tp.camel;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;
import tp.camel.strategies.AnimalAggregationStrategy;
import tp.camel.strategies.CageAggregationStrategy;
import tp.camel.strategies.CenterAggregationStrategy;
import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;

@Component
public class CamelRoutes extends RouteBuilder {

    private static final String SERVICE1_URL = "localhost:8081/zoo-manager/";
    private static final String SERVICE2_URL = "localhost:8082/zoo-manager/";
    private static final String SERVICE3_URL = "localhost:8083/zoo-manager/";

    @Override
    public void configure() throws Exception {

        /**
         * GET method bound to calls on /animals
         */
        from("direct:FindAllAnimals")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.HTTP_PATH, simple("animals"))
                .multicast(new CenterAggregationStrategy()).parallelProcessing()
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .unmarshal().json(JsonLibrary.Jackson, Center.class)
                .end();

        /**
         * GET method bound to calls on /animals/{something}
         */
        from("direct:FindAnimalById")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.HTTP_PATH, simple("animals/${property.id}"))
                .multicast(new AnimalAggregationStrategy()).parallelProcessing()
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .unmarshal().json(JsonLibrary.Jackson, Animal.class)
                .end();

        /**
         * GET method bound to calls on /find/byName{something}
         */
        from("direct:FindAnimalByName")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.HTTP_PATH, simple("find/byName/${property.name}"))
                .multicast(new AnimalAggregationStrategy()).parallelProcessing()
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .unmarshal().json(JsonLibrary.Jackson, Animal.class)
                .end();

        /**
         * GET method bound to calls on /find/at/{something}
         */
        from("direct:FindAnimalAtPosition")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.HTTP_PATH, simple("find/at/${property.position}"))
                .multicast(new AnimalAggregationStrategy()).parallelProcessing()
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .unmarshal().json(JsonLibrary.Jackson, Animal.class)
                .end();

        /**
         * GET method bound to calls on /find/near/{something}
         */
        from("direct:FindAnimalNearPosition")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.HTTP_PATH, simple("find/near/${property.position}"))
                .multicast(new CageAggregationStrategy()).parallelProcessing()
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .unmarshal().json(JsonLibrary.Jackson, Cage.class)
                .end();

        /**
         * GET method bound to calls on /animals/{something}/wolf
         */
        from("direct:GetInformation")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.HTTP_PATH, simple("animals/${property.id}/wolf"))
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .end();

        /**
         * GET method bound to calls on /cages/{something}
         */
        from("direct:FindCageByName")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.HTTP_PATH, simple("cages/${property.name}"))
                .multicast(new CageAggregationStrategy()).parallelProcessing()
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .unmarshal().json(JsonLibrary.Jackson, Cage.class)
                .end();

        /**
         * GET method bound to calls on /center/journey/from/{route}
         */
        from("direct:GetRouteInformation")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.HTTP_PATH, simple("center/journey/from/${property.route}"))
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .end();

        /**
         * POST method bound to calls on /animals
         */
        from("direct:CreateAnimal")
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/xml"))
                .setBody(simple("${body}"))
                .setHeader(Exchange.HTTP_PATH, simple("animals"))
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .end();

        /**
         * POST method bound to calls on /cages
         */
        from("direct:CreateCage")
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/xml"))
                .setBody(simple("${property.cage}"))
                .setHeader(Exchange.HTTP_PATH, simple("cages"))
                .multicast(new AnimalAggregationStrategy()).parallelProcessing()
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .end();

        /**
         * PUT method bound to calls on /animals/{something}
         */
        from("direct:UpdateAnimalById")
                .setHeader(Exchange.HTTP_METHOD, constant("PUT"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/xml"))
                .setBody(simple("{property.animal}"))
                .setHeader(Exchange.HTTP_PATH, simple("animals/${property.id}"))
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .end();

        /**
         * PUT method bound to calls on /animals
         */
        from("direct:UpdateAllAnimals")
                .setHeader(Exchange.HTTP_METHOD, constant("PUT"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/xml"))
                .setBody(simple("{property.animal}"))
                .setHeader(Exchange.HTTP_PATH, simple("animals/"))
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .end();

        /**
         * PUT method bound to calls on /cages/{something}
         */
        from("direct:UpdateCageByName")
                .setHeader(Exchange.HTTP_METHOD, constant("PUT"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/xml"))
                .setBody(simple("{property.cage}"))
                .setHeader(Exchange.HTTP_PATH, simple("cages/${property.name}"))
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .end();

        /**
         * PUT method bound to calls on /cages
         */
        from("direct:UpdateAllCages")
                .setHeader(Exchange.HTTP_METHOD, constant("PUT"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/xml"))
                .setBody(simple("{property.cage}"))
                .setHeader(Exchange.HTTP_PATH, simple("cages"))
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .end();

        /**
         * DELETE method bound to calls on /animals/{something}
         */
        from("direct:DeleteAnimalById")
                .setHeader(Exchange.HTTP_METHOD, constant("DELETE"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/xml"))
                .setHeader(Exchange.HTTP_PATH, simple("animals/${property.id}"))
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .end();

        /**
         * DELETE method bound to calls on /animals
         */
        from("direct:DeleteAllAnimals")
                .setHeader(Exchange.HTTP_METHOD, constant("DELETE"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/xml"))
                .setHeader(Exchange.HTTP_PATH, simple("animals"))
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .end();

        /**
         * DELETE method bound to calls on /cages/{something}
         */
        from("direct:DeleteCageByName")
                .setHeader(Exchange.HTTP_METHOD, constant("DELETE"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/xml"))
                .setHeader(Exchange.HTTP_PATH, simple("cages/${property.name}"))
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .end();

        /**
         * DELETE method bound to calls on /cages
         */
        from("direct:DeleteAllCages")
                .setHeader(Exchange.HTTP_METHOD, constant("DELETE"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/xml"))
                .setHeader(Exchange.HTTP_PATH, simple("cages"))
                .enrich(SERVICE1_URL)
                .enrich(SERVICE2_URL)
                .enrich(SERVICE3_URL)
                .end();
    }
}