package tp.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;
import java.util.Objects;

@XmlRootElement
public class Cage {
    /**
     * The name of this cage
     */
    private String name;
    /**
     * The visitor entrance location
     */
    private Position position;
    /**
     * The maximum number of animals in this cage
     */
    private Integer capacity;
    /**
     * The animals in this cage.
     */
    private Collection<Animal> residents;

    public Cage() {
    }

    public Cage(String name, Position position, Integer capacity, Collection<Animal> residents) {
        this.name = name;
        this.position = position;
        this.capacity = capacity;
        this.residents = residents;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Cage{" +
                "name='" + name + '\'' +
                ", position=" + position +
                ", capacity=" + capacity +
                ", residents=" + residents +
                '}';
    }

    public Integer getCapacity() {
        return capacity;
    }

    public Position getPosition() {
        return position;
    }

    public Collection<Animal> getResidents() {
        return residents;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setResidents(Collection<Animal> residents) {
        this.residents = residents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cage cage = (Cage) o;
        return Objects.equals(name, cage.name) &&
                Objects.equals(position, cage.position) &&
                Objects.equals(capacity, cage.capacity) &&
                Objects.equals(residents, cage.residents);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, position, capacity, residents);
    }
}
