package eip;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.log4j.BasicConfigurator;

import java.util.Scanner;

public class ProducerConsumer3 {
    public static void main(String[] args) throws Exception {
        // Configure le logger par défaut
        BasicConfigurator.configure();
        // Contexte Camel par défaut
        CamelContext context = new DefaultCamelContext();
        // Crée une route contenant le consommateur
        RouteBuilder routeBuilder = new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                // On définit un consommateur 'zoo-consumer'
                from("direct:zoo-consumer")
                        .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                        .setHeader(Exchange.HTTP_PATH, simple("find/byName/${property.name}"))
                        .to("https://zoo-manager-rest.eu-gb.mybluemix.net/zoo-manager")
                        .log("reponse received : ${body}");
            }
        };
        // On ajoute la route au contexte
        routeBuilder.addRoutesToCamelContext(context);

        // On démarre le contexte pour activer les routes
        context.start();

        // On crée un producteur
        ProducerTemplate pt = context.createProducerTemplate();

        // On récupère le nom de l'animal recherché
        System.out.print("Nom de l'animal recherché : ");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();

        // On execute la requête
        pt.send("direct:zoo-consumer", exchange -> exchange.setProperty("name", name));
    }
}